import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabmarkComponent } from './labmark.component';

describe('LabmarkComponent', () => {
  let component: LabmarkComponent;
  let fixture: ComponentFixture<LabmarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabmarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
