import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabdevComponent } from './labdev.component';

describe('LabdevComponent', () => {
  let component: LabdevComponent;
  let fixture: ComponentFixture<LabdevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabdevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabdevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
