import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabdesComponent } from './labdes.component';

describe('LabdesComponent', () => {
  let component: LabdesComponent;
  let fixture: ComponentFixture<LabdesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabdesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabdesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
