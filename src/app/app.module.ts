import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Component/header/header.component';
import { FooterComponent } from './Component/footer/footer.component';
import { LabdevComponent } from './Component/labdev/labdev.component';
import { LabmarkComponent } from './Component/labmark/labmark.component';
import { LabdesComponent } from './Component/labdes/labdes.component';
import { ContactComponent } from './Component/contact/contact.component';
import { AccueilComponent } from './Component/accueil/accueil.component';
import appRoutes from './routing/routerConfig';
import { ActualiteComponent } from './Component/actualite/actualite.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LabdevComponent,
    LabmarkComponent,
    LabdesComponent,
    ContactComponent,
    AccueilComponent,
    ActualiteComponent
  ],
  imports: [
    BrowserModule,RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
