import { Routes } from '@angular/router';
import { ContactComponent } from '../Component/contact/contact.component';
import { AccueilComponent } from '../Component/accueil/accueil.component';
import { LabdevComponent } from '../Component/labdev/labdev.component';
import { LabmarkComponent } from '../Component/labmark/labmark.component';
import { LabdesComponent } from '../Component/labdes/labdes.component';
import { ActualiteComponent } from '../Component/actualite/actualite.component';

const appRoutes: Routes = [
  { path: '',
    component: AccueilComponent
  },
  {
    path: 'labdev',
    component: LabdevComponent
  },
  {
    path: 'labmark',
    component: LabmarkComponent
  },
  {
    path: 'labdes',
    component: LabdesComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'actualite',
    component: ActualiteComponent
  },
];
export default appRoutes;
